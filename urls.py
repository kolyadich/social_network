from basis import views
from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^index/', views.index, name='index'),
    url(r'^groups/', views.groups, name='groups'),
    url(r'^members/', views.members, name='members'),
    url(r'^photos/', views.photos, name='photos'),
    url(r'^profile/', views.profile, name='profile'),

    url(r'^admin/', include(admin.site.urls)),
]
